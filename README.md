[![License: BSD](https://img.shields.io/badge/license-BSD%203--clause-blue)](https://opensource.org/licenses/BSD-3-Clause)

# BMP heuristic in C

The C implementation of the Boyar, Matthews, Peralta heuristic.  Please, refer
to the original [article](https://doi.org/10.1007/s00145-012-9124-7).

## Compile

You just need to type and execute

`gcc -o minimixor minimixor.c`

and that's all.

## Xor system format

For instance, put

```
11 17
10000000000000000
11100000000000000
00111000000000000
01010110100101101
01100101010101101
11000011001111000
00101111000011110
01111110100011000
00000000100000101
00000010001000100
00000000001000000

```

in a file called `test_0.txt`. Please, put a last blank line at the end.
Meaning of the file is quite clear. It is a linear system with 11 equations and
17 variables. 

## Usage

Type

`./minimixor test_0.txt`

and you will see the evolution of the solution. There are also 4 options for
different breaking criterion:

1. `-n` using the norm;
2. `-l` using the norm-largest;
3. `-d` using the norm-diff;
4. `-r` using the random.

The default behaviour is `-n`. For istance you could run

`./minimixor -l test_0.txt`

and get a different (worse) result.

## To do

Add support for variables symbols.

