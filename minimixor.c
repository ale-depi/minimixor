/* Author: Alessandro De Piccoli. */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <getopt.h>

void print_array(unsigned char *array, unsigned char length) {
	unsigned char i;
	printf("[");
	for (i = 0; i < length - 1; i++)
		printf("%d, ", array[i]);
	printf("%d]", array[length - 1]);
}

unsigned char is_in_array(uint64_t entry, uint64_t *array, unsigned char length) {
	unsigned char i;
	for (i = 0; i < length; i++)
		if (array[i] == entry)
			return 1;
	return 0;
}

uint64_t get_norm_1(unsigned char *vector, unsigned char card_of_vector) {
	uint64_t norm_1 = 0;
	unsigned char i;
	for (i = 0; i < card_of_vector; i++)
		norm_1 += vector[i];
	return norm_1;
}

uint64_t get_norm_2(unsigned char *vector, unsigned char card_of_vector) {
	uint64_t norm_2 = 0;
	unsigned char i;
	for (i = 0; i < card_of_vector; i++)
		norm_2 += vector[i] * vector[i];
	return norm_2;
}

/* Norm */
char norm(unsigned char *vector, unsigned char card_of_vector, uint64_t norm_2) {
	uint64_t temp_norm_2 = get_norm_2(vector, card_of_vector);
	if (temp_norm_2 > norm_2)
		return 1;
	return 0;
}

/* Norm-largest */
char norm_largest(unsigned char *vector, unsigned char card_of_vector, uint64_t norm_2) {
	uint64_t temp_norm_2 = get_norm_2(vector, card_of_vector), largest = vector[0];
	unsigned char i;
	for (i = 1; i < card_of_vector; i++)
		if (vector[i] > largest)
			largest = vector[i];
	if (temp_norm_2 - largest > norm_2)
		return 1;
	return 0;
}

/* Norm-diff */
char norm_diff(unsigned char *vector, unsigned char card_of_vector, uint64_t norm_2) {
	uint64_t temp_norm_2 = get_norm_2(vector, card_of_vector), largest = vector[0], largest_2 = vector[0];
	unsigned char i;
	for (i = 1; i < card_of_vector; i++)
		if (vector[i] > largest)
			largest = vector[i];
	for (i = 1; i < card_of_vector; i++)
		if (vector[i] < largest && vector[i] > largest_2)
			largest_2 = vector[i];
	if (temp_norm_2 - (largest - largest_2) > norm_2)
		return 1;
	return 0;
}

/* Random */
char random_crit(unsigned char *vector, unsigned char card_of_vector, uint64_t norm_2) {
	srand(time(NULL));
	if (rand() % 2 == 0)
		return 0;
	else
		return norm(vector, card_of_vector, norm_2);
}

uint64_t binomial(unsigned char n, unsigned char k) {
	unsigned char i;
	uint64_t num = 1, den = 1;
	for (i = n; i >= (n - k + 1); i--)
		num *= i;
	for (i = k; i >= 2; i--)
		den *= i;
	return num / den;
}

void get_distances(unsigned char *new_distances, uint64_t *xor_system, unsigned char *distances, uint64_t *signals, uint64_t new_signal, unsigned char rows, unsigned char cols, unsigned char num_of_signals) {
	uint64_t signals_from_combs[rows];
	uint64_t i, n_of_combs, temp_signal, norm_1;
	unsigned char combination[cols], marks[rows], max[cols];
	unsigned char curr_distance, j, k, start;
	for (i = 0; i < rows; i++) {
		new_distances[i] = distances[i];
		marks[i] = 0;
	}
	for (i = 0; i < rows; i++)
		if (distances[i] == 0)
			marks[i] = 1;
	for (i = 0; i < rows; i++) {
		if (distances[i] == 1) {
			new_distances[i] = (xor_system[i] == new_signal) ? 0 : 1;
			marks[i] = 1;
		}
	}
	norm_1 = get_norm_1(marks, rows);
	while (norm_1 != rows) {
		for (i = 0; i < rows; i++)
			signals_from_combs[i] = 0;
		for (i = 0; i < rows; i++)
			if (marks[i] == 0)
				break;
		curr_distance = distances[i];
		n_of_combs = binomial(num_of_signals, curr_distance - 1);
		for (i = 0; i < (curr_distance - 1); i++) {
			combination[i] = i;
			max[i] = num_of_signals - curr_distance + 1 + i;
		}
		k = 0;
		for (i = 0; i < n_of_combs; i++) {
			temp_signal = 0;
			for (j = 0; j < curr_distance - 1; j++)
				temp_signal ^= signals[combination[j]];
			temp_signal ^= new_signal;
			if (is_in_array(temp_signal, xor_system, rows) && !is_in_array(temp_signal, signals_from_combs, rows)) {
				signals_from_combs[k] = temp_signal;
				k++;
			}
			start = curr_distance - 2;
			while (combination[start] == max[start] && start > 0)
				start--;
			combination[start]++;
			for (j = start + 1; j < curr_distance - 1; j++)
				combination[j] = combination[j - 1] + 1;
		}
		for (i = 0; i < rows; i++) {
			if (distances[i] == curr_distance) {
				if (is_in_array(xor_system[i], signals_from_combs, k)) {
					new_distances[i] -= 1;
					marks[i] = 1;
				}
				else
					marks[i] = 1;
			}
		}
		norm_1 = get_norm_1(marks, rows);
	}
}

int main(int argc, char *argv[]) {
	FILE *input;
	uint64_t *xor_system, *signals, stored_signal, temp_signal, tmp;
	uint64_t norm_1, norm_2, temp_norm_1;
	unsigned char *distances, *stored_distances, *temp_distances;
	unsigned char cols, j, k, num_of_signals, rows;
	char i, one_flag, use_break;
	char (*break_criterion)(unsigned char *, unsigned char, uint64_t) = norm;
	int opt;

	while ((opt = getopt(argc, argv, "nldr")) != -1) {
		switch (opt) {
			case 'n':
				break_criterion = norm;
				break;
			case 'l':
				break_criterion = norm_largest;
				break;
			case 'd':
				break_criterion = norm_diff;
				break;
			case 'r':
				break_criterion = random_crit;
				break;
			default: /* '?' */
			fprintf(stderr, "Usage: %s [-nldr] file\n", argv[0]);
			fprintf(stderr, "The default is n (norm), choose just ONE!\n");
			exit(EXIT_FAILURE);
		}
	}

	/* getting system to minimize
	 * computing initial distances */
	input = fopen(argv[optind], "r");
	fscanf(input, "%hhd", &rows);
	fscanf(input, "%hhd", &cols);
	xor_system = (uint64_t*)calloc(rows, sizeof(uint64_t));
	distances = (unsigned char*)calloc(rows, sizeof(unsigned char));
	fgetc(input);
	printf("\nSize of system is:\n");
	printf("%d x %d\n", rows, cols);
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			tmp = fgetc(input);
			tmp -= 48;
			distances[i] += tmp;
			tmp <<= (cols - j - 1);
			xor_system[i] ^= tmp;
		}
		fgetc(input); /* to consume \n */
	}
	fclose(input);
	for (i = 0; i < rows; i++)
		distances[i] -= 1;
	printf("\nInitial distances are:\n");
	print_array(distances, rows);
	printf("\n");

	norm_1 = get_norm_1(distances, rows);
	norm_2 = get_norm_2(distances, rows);

	/* signals are initially the canonical basis */
	signals = (uint64_t*)calloc(100, sizeof(uint64_t));
	for (i = 0; i < cols; i++)
		signals[i] = 1 << i;

	/* main loop*/
	num_of_signals = cols;
	stored_distances = (unsigned char*)malloc(rows * sizeof(unsigned char));
	temp_distances = (unsigned char*)malloc(rows * sizeof(unsigned char));
	printf("\nDistances evolution is:\n");
	while (norm_1 != 0) {
		one_flag = -1;
		for (i = 0; i < rows && one_flag == -1; i++)
			if(distances[i] == 1)
				one_flag = i;
		if (one_flag != -1) {
			stored_signal = xor_system[one_flag];
			get_distances(stored_distances, xor_system, distances, signals, stored_signal, rows, cols, num_of_signals);
			norm_1 = get_norm_1(stored_distances, rows);
			norm_2 = get_norm_2(stored_distances, rows);
		}
		else {
			for (i = 0; i < num_of_signals - 1; i++) {
				for (j = i + 1; j < num_of_signals; j++) {
					temp_signal = signals[i] ^ signals[j];
					get_distances(temp_distances, xor_system, distances, signals, temp_signal, rows, cols, num_of_signals);
					temp_norm_1 = get_norm_1(temp_distances, rows);
					use_break = break_criterion(temp_distances, rows, norm_2);
					if (temp_norm_1 < norm_1 || (temp_norm_1 == norm_1 && use_break)) {
						norm_1 = temp_norm_1;
						norm_2 = get_norm_2(temp_distances, rows);
						stored_signal = temp_signal;
						for (k = 0; k < rows; k++) {
							stored_distances[k] = temp_distances[k];
						}
					}
				}
			}
		}
		for (i = num_of_signals; i > 0; i--)
			signals[i] = signals[i - 1];
		signals[0] = stored_signal;
		num_of_signals++;
		for (i = 0; i < rows; i++)
			distances[i] = stored_distances[i];
		print_array(distances, rows);
		printf(" \t");
		for (i = cols - 1; i >= 0; i--)
			printf("%ld", (stored_signal >> i) & 1);
		printf("\n");
	}

	printf("\nNumber of bit operations is:\n%d\n\n", num_of_signals - cols);

	/* free allocated memory */
	free(distances);
	free(signals);
	free(stored_distances);
	free(temp_distances);
	free(xor_system);
	return 0;
}

